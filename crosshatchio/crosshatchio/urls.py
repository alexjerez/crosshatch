from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from posts.views import PostListView, PostDetailView, list_posts
from django.contrib.auth.decorators import login_required
from profiles.views import UserProfileDetailView
#from posts.forms import PostForm

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
#url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    # PHILEO
    url(r'^likes/', include("phileo.urls")),
    url(r'^$', list_posts, name='home'),
    url(r'^top/', list_posts, {"sortby" : "top"}, name='home_top'),

    # Examples:
    # url(r'^$', 'crosshatchio.views.home', name='home'),
    # url(r'^crosshatchio/', include('crosshatchio.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
   # url(r'^bookmarks/', include('bookmarks.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^listposts/', login_required(PostListView.as_view()),\
        name='listposts'),
    #url(r'accounts/', include(accounts.urls), name='accounts'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^favorite/(?P<postdata>[\w]+)/$', view='posts.views.favorite_post', name='toggle_favorite'),
#url(r'^newpost/$', MakePost.as_view(template_name='newpost2.html'), name='newpost'),
    url(r'^newpost/$', 'posts.views.MakePost', name='newpost',),
    url(r'^tags/(?P<tag>[-\w]+)/$', view='posts.views.tag_view', name='tagview'),
    url(r'^tags/(?P<tag1>[-\w]+)/(?P<tag2>[-\w]+)/$', view='posts.views.tags_view', name='2tagsview'),
    url(r'^tags/(?P<tag1>[-\w]+)/(?P<tag2>[-\w]+)/(?P<tag3>[-\w]+)/$', view='posts.views.threetags_view', name='3tagsview'),
    url(r'^user/(?P<slug>\w+)/$', UserProfileDetailView.as_view(), name='profile'),
    url(r'^testy/$', TemplateView.as_view(template_name='testy.html'), name='test1'),
    url(r'^post/(?P<slug>[-\w]+)/$', PostDetailView.as_view(), name='postdetails'),
)
