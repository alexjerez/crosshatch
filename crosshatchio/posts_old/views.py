from django.views.generic import ListView, DetailView
from django.forms.models import modelformset_factory
from django.shortcuts import render_to_response


from models import Post


class PostListView(ListView):
    model = Post

class PostDetailView(DetailView):
    model = Post

def submit_post(request):
    PostFormSet = modelformset_factory(Post)
    if request.method == 'POST':
        formset = PostFormSet(request.POST)
        if formset.is_valid():
            formset.save()

    else:
        formset = PostFormSet()
    return render_to_response("newpost2.html", {"form":formset,})

