from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from posts.models import Post

class SiteUserManager(UserManager):
   pass

class SiteUser(AbstractUser):
    #points = models.PositiveIntegerField(_("points"), default=0, blank=True)
    objects = SiteUserManager()
    favorites = models.ManyToManyField(Post, related_name='favorited_by')
