from django.views.generic import ListView, DetailView
from django.contrib.auth import get_user_model
from .models import SiteUser


class UserProfileDetailView(DetailView):
    model = get_user_model()
    slug_field = "username"
    template_name = "profiles/user_detail.html"
