from django.contrib import admin
from .models import SiteUser

class SiteUserAdmin(admin.ModelAdmin):
    pass
admin.site.register(SiteUser, SiteUserAdmin)
