from django.contrib import admin
from .models import TagSet, Tag

admin.site.register(TagSet)
admin.site.register(Tag)
