from django.db import models

class Tag(models.Model):
    tag = models.CharField(max_length=30, default='misc', unique=True)

    def __unicode__(self):
        return self.tag


class TagSet(models.Model):
    tag1 = models.ForeignKey(Tag, related_name='tag1+')
    tag2 = models.ForeignKey(Tag, related_name='tag2+') 
    tag3 = models.ForeignKey(Tag, related_name='tag3+')

    def add_tags(self, first, second, third):
        pass
    def __unicode__(self):
        return '%s+%s+%s' % tag1, tag2, tag3 


# Create your models here.
