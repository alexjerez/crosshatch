from django.views.generic import ListView, DetailView
from .models import EntryPost, EntryLink, EntryPic
from itertools import chain

#EntryPost
class EntryPostView(ListView):
    model = EntryPost

class EntryPostDetailView(DetailView):
    model = EntryPost

#EntryLink
class EntryLinkView(ListView):
    model = EntryLink

class EntryLinkDetailView(DetailView):
    model = EntryLink

#EntryPic
class EntryPicView(ListView):
    model = EntryPic

class EntryPicDetailView(ListView):
    model = EntryPic
#All
class AllEntriesView(ListView):
    posts = EntryPost.objects.all()
    links = EntryLink.objects.all()
    pics = EntryPic.objects.all() 
    queryset  = chain(posts, links, pics)
