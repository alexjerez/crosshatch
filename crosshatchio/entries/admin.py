from django.contrib import admin
from .models import EntryText, EntryLink, EntryPic

class PostInline(admin.StackedInline):
    model = EntryText
class LinkInline(admin.StackedInline):
    model = EntryLink
class PicInline(admin.StackedInline):
    model = EntryPic

class EntryAdmin(admin.ModelAdmin):
    inlines = [ PostInline, ]

admin.site.register(EntryText, EntryAdmin)
