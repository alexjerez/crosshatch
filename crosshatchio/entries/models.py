from django.db import models
from posts.models import Post, AbstractPost, LinkPostMixin

class EntryText(Post):
    pass

class EntryLink(AbstractPost, LinkPostMixin):
    pass

class EntryPic(AbstractPost, LinkPostMixin):
    pass
