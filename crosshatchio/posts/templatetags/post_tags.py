from django import template

register = template.Library()

@register.filter
def domain(value):
    """returns the root domain of the url of the given post"""
    return value.get_domain()

