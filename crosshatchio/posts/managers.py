from django.db import models
from phileo.models import Like
from django.db.models import Count


class PostVoteCountManager(models.Manager):
    def get_query_set(self):
        return super(PostVoteCountManager, self).get_query_set().annotate(
            votes=Count('vote')).order_by("-votes")

class PostManager(models.Manager):
    pass
