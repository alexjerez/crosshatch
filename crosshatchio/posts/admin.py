from django.contrib import admin
from .models import Post
from django.forms import ModelForm
from django.core.exceptions import ValidationError

class PostAdminForm(ModelForm):
    class Meta:
        model = Post

    def clean_tags(self):
        tags = self.cleaned_data['tags']
        if len(tags) > 3:
            raise ValidationError("3 tags maximun per post")
        return tags

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    form = PostAdminForm

admin.site.register(Post, PostAdmin)
