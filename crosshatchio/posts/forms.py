from django.forms import ModelForm
from .models import Post
from django.core.exceptions import ValidationError


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'url', 'text', 'tags', 'is_nsfw', 'is_anonymous']

    def clean_tags(self):
        tags = self.cleaned_data['tags']
        for tag in tags:
            tag = tag.lower()
        if len(tags) > 3:
            raise ValidationError("You fucked up.")
        return tags


