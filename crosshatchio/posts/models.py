from django.db import models
from .managers import PostManager, PostVoteCountManager
from django.conf import settings
from taggit.managers import TaggableManager
from django.utils.text import slugify
from django.contrib.contenttypes.models import ContentType
from phileo.models import Like
import urlparse
import shortuuid
##from bookmarks.handlers import library
#from autoslug import AutoSlugField

# Create your models here.
class Post(models.Model):


    title = models.CharField(max_length=200)
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    created = models.DateTimeField(auto_now_add=True)
    url = models.URLField()
    text = models.TextField(max_length=600, blank=True, null=True)
    tags = TaggableManager()
    slug = models.SlugField()
    objects = PostManager()
    is_nsfw = models.BooleanField(default=False)
    is_anonymous = models.BooleanField(default=False)
    score = models.IntegerField(default=0)
    uuid = shortuuid.uuid()
#    with_votes = PostVoteCountManager()


    def get_likes(self):
        return Like.objects.filter(
            #receiver_content_type=ContentType.objects.get_for_model(self),
            receiver_object_id=self.pk
        ).count()


    def get_domain(self):
        if self.url:
            hostname = urlparse.urlparse(self.url).hostname.split(".")
            hostname = ".".join(len(hostname[-2]) < 4
                                and hostname[-3:] or hostname[-2:])
            return hostname
        else:
            print('Unable to get Domain')
            pass




    class Meta:
        ordering = ["-created"]
        verbose_name = "post"



    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title


'''
class Vote(models.Model):
    voter = models.ForeignKey(settings.AUTH_USER_MODEL)
    post = models.ForeignKey(Post)
    def __unicode__(self):
        return "%s voted \'%s\'" % (self.voter.username,self.post.title)
'''
##library.register(Post)
