from django.views.generic import ListView, DetailView, CreateView, FormView
from django.forms.models import modelformset_factory
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.template import RequestContext
from models import Post
from .forms import PostForm
# start makepost imports
from django.http import HttpResponseRedirect
from taggit.managers import TaggableManager
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required

class PostListView(ListView):
    model = Post

class PostDetailView(DetailView):
    model = Post


def list_posts(request, sortby='newest'):
    if request.user.is_authenticated():
        if request.method == 'GET':
            if sortby == 'newest':
                post_list = Post.objects.all()
            elif sortby == 'top':
                post_list = Post.objects.all()
                for post in post_list:
                    post.score = post.get_likes()
                post_list.order_by('-score')
            '''else if sortby == 'starred':
                post_list = Post.objects.filter(author=request.user).order_by('created')
            else if sortby == 'tags':
                post_list = Post.objects.filter('''
            paginator = Paginator(post_list, 25)

            page = request.GET.get('page')
            try:
                posts = paginator.page(page)
                attempt = 1
            except PageNotAnInteger:
                posts = paginator.page(1)
                attempt = 2
            except EmptyPage:
                posts = paginator.page(paginator.num_pages)
                attempt = 3

            context = {'posts': posts}
            print("\n This worked at attempt #%s \n" % attempt )
            return render_to_response('listposts.html', context, context_instance=RequestContext(request))
    else:
        return render(request, 'splash.html')


@login_required
def tag_view(request, tag):
    if request.method == 'GET':
        t = tag
        q = Post.objects.filter(tags__name__in=[t])
        context = {
            'tagged_posts': q,
            'selected_tag': t,
        }
        return render_to_response('taglist.html', context, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect(reverse('auth_login'))

@login_required
def tags_view(request, tag1, tag2):
    if request.method == 'GET':
        t1 = tag1
        t2 = tag2
# queries t1 AND t2
        q = Post.objects.filter(tags__name__in=[t1]).filter(tags__name__in=[t2])
# queries t1 OR t2
#q = Post.objects.filter(tags__name__in=[t1, t2]).distinct()
        context = {
            'tagged_posts': q,
            'selected_tags': [t1, t2],
        }
        return render_to_response('tagslist.html', context, context_instance=RequestContext(request))
    else:
        return HTTPResponseRedirect(reverse('auth_login'))


def favorite_post(request, postdata):
    k = Post.objects.get(pk=postdata)
    print("Trying... ")
    try:
        request.user.favorites.add(k)
        print("added!")
    except:
        print(" adding %s..." % k.title)
        print("\'favoritepost\' did not work")
    return redirect("home")



@login_required
def threetags_view(request, tag1, tag2, tag3):
    if request.method == 'GET':
        t1 = tag1
        t2 = tag2
        t3 = tag3
# queries t1 AND t2 AND t3
        q = Post.objects.filter(tags__name__in=[t1]).filter(tags__name__in=[t2]).filter(tags__name__in=[t3])
# queries t1 OR t2
#q = Post.objects.filter(tags__name__in=[t1, t2]).distinct()
        context = {
            'tagged_posts': q,
            'selected_tags': [t1, t2, t3],
        }
        return render_to_response('3tagslist.html', context, context_instance=RequestContext(request))
    else:
        return HTTPResponseRedirect(reverse('auth_login'))


@login_required
def MakePost(request):
# This is used in 'newpost.html'...
    if request.method == 'GET':
        if request.user.is_authenticated():
            form = PostForm()
            context = {'form': form}
            return render_to_response('newpost.html', context,
                                    context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect(reverse('auth_login'))
    if request.method == 'POST':
        form = PostForm(request.POST)
        context = {'form': form}
        if form.is_valid():
            #taginput = TaggableManager()
            #taginput.tags.add(str(form.cleaned_data['tags']))
            taginput = form.cleaned_data['tags']
            newpost = Post(title=form.cleaned_data['title'], author=request.user, url=form.cleaned_data['url'], \
                        text=form.cleaned_data['text'], is_nsfw=form.cleaned_data['is_nsfw'], \
                        is_anonymous=form.cleaned_data['is_anonymous'])
            #newpost = form.save(commit=False)
            newpost.save()
            for tag in taginput:
                newpost.tags.add(tag)
            #form.save_m2m()
            return HttpResponseRedirect(reverse('home'))
            #return render_to_response('newpost.html', context, context_instance=RequestContext(request))
        else:
            return render_to_response('newpost.html', context, context_instance=RequestContext(request))
'''
class MakePost(CreateView):
    model = Post
#    template_name = 'newpost.html'
    form_class = PostForm
    success_url = '/'
#    fields = ['title', 'url', 'text', 'tags']'''
'''    PostFormSet = modelformset_factory(Post)
    if request.method == 'POST':
        formset = PostFormSet(request.POST)
        if formset.is_valid():
            formset.save()

    else:
        formset = PostFormSet()
    return render_to_response("newpost.html", {"form":formset,},
            RequestContext(request))
class VoteFormView(FormView):
    form_class = VoteForm

    def form_valid(self, form):
        print("starting orm_valid.")
        post = get_object_or_404(Post, pk=form.data["post"])
        user = self.request.user
        prev_votes = Vote.objects.filter(voter=user, post=post)
        has_voted = (prev_votes.count() > 0)

        if not has_voted:
            # add vote
            Vote.objects.create(voter=user, post=post)
            print("voted")
        else:
            prev_votes[0].delete()
            print("unvoted")
        return redirect("home")

    def form_invalid(self,form):
        print("form invalid")
        redirect("home")
'''
